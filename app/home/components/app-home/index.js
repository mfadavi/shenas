var home = angular.module('app.home');

home.component('appHome', {
	template: require('./app-home.html'),
	controller: HomeCtrl,
	bindings: { layout: '@', flex: '@' }
});
HomeCtrl.$inject = ['$scope', '$timeout', 'appNationalIdManager', '$log'];
function HomeCtrl ($scope, $timeout, appNationalIdManager, $log) {
	var self = this;
	var UNKNOWN = 'نامشخص';
	var DELAY = 500; // milliseconds
	var process = null;

	/* instance properties */
	
	self.$onInit = function () {
		!self.layout && (self.layout = 'column');
		!self.flex && (self.flex = '');
	};

	/* $scope properties */

	$scope.nationalId = '';

	$scope.nationalIdChanged = function (e) {
		$log.debug('changed! ' + $scope.nationalId);
		cancelProcess();
		
		if (!$scope.nationalId) {
			$scope.status = null;
		} else {
			$scope.status = 'processing';
			process = $timeout(doValidation, DELAY);
		}
	};

	/* functions */

	function doValidation () {
		var digits = appNationalIdManager.parse($scope.nationalId);

		if (digits.error) {
			$scope.status = null;
			return;
		}
		
		var parts = appNationalIdManager.parseParts(digits.value);
		var location = appNationalIdManager.findLocation(parts.cityCode);
		var isValid = appNationalIdManager.validate(digits.value);

		$scope.isValid = isValid;
		$scope.city = location.city || UNKNOWN;
		$scope.province = location.province || UNKNOWN;
		$scope.status = 'ready';
	}

	function cancelProcess () {
		if (process) {
			$timeout.cancel(process);
			process = null;
		}
	}
	
}
