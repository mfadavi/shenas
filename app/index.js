require('angular/angular');
require('angular-animate/angular-animate');
require('angular-aria/angular-aria');
require('angular-messages/angular-messages');
require('@uirouter/angularjs/release/angular-ui-router');
require('angular/angular-csp.css');

/* Modules: begin */
require('./common');
require('./home');
require('./about');
require('./browse');
/* Modules: end */

var app = angular.module('app', [
	'ngMessages',
	'ui.router',
	'app.common',
	'app.home',
	'app.about',
	'app.browse'
]);

app.config(configStates);
configStates.$inject = ['$stateProvider'];
function configStates ($stateProvider) {
	
	$stateProvider.state({
		name: 'app',
		url: '',
		component: 'appBase',
		redirectTo: 'app.home'
	});

	$stateProvider.state({
		name: 'app.test',
		url: '/test',
		template: '<div style="background:red" layout="column" flex>CONTENT</div>'
	});
	
	$stateProvider.state({
		name: 'app.home',
		url: '/home',
		component: 'appHome'
	});

	$stateProvider.state({
		name: 'app.browse',
		url: '/browse',
		component: 'appBrowse'
	});

	$stateProvider.state({
		name: 'app.about',
		url: '/about',
		component: 'appAbout'
	});
	
}

app.run(exposeState);
exposeState.$inject = ['$rootScope', '$state', '$stateParams'];
function exposeState ($rootScope, $state, $stateParams) {
	$rootScope.$state = $state;
	$rootScope.$params = $stateParams;
}
