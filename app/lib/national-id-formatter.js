const translateNumber = require('./translate-number');
const parseDigits = require('./parse-digits');

const PATTERN = /[0-9]{1,3}-?[0-9]{6}-?[0-9]/;
const ALL_DASHES = /-/g;

function parse (input) {
	if (typeof input === 'number' && Number.isSafeInteger(input)) {
		return parseDigits(input);
	}

	if (typeof input === 'string') {
		input = translateNumber.toEnglish(input.trim());

		if (!seemsValid(input)) {
			return { value: null, error: 'invalid-input' };
		}

		input = input.replace(ALL_DASHES, '');
		input = Number.parseInt(input);
		
		return { value: parseDigits(input), error: null };
	}

	return { value: null, error: 'invalid-input' };
}

function seemsValid (str) {
	return PATTERN.test(str);
}

function parseParts (digits) {
	const len = digits.length;

	const cityCode = digits.slice(0, len - 7).join('');
	const unique = digits.slice(len - 7, len - 1).join('');
	const checksum = digits[len - 1] + '';
	
	return {checksum, unique, cityCode};
}

function dashify (...peers) {
	return peers.join('-');
}

function format (digits) {
	const parts = parseParts(digits);
	return dashify(
		translateNumber.toPersian(parts.cityCode),
		translateNumber.toPersian(parts.unique),
		translateNumber.toPersian(parts.checksum)
	);
}

module.exports = { parse, seemsValid, parseParts, dashify, format };
