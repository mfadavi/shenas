const {provinces, cities} = require('./code-data.json');

function findCity (code) {
	for (let city of cities) {
		if (~city.code.indexOf(code)) {
			return city;
		}
	}

	return null;
}

function findProvince (provinceId) {
	for (let province of provinces) {
		if (province.id === provinceId) {
			return province;
		}
	}

	return null;
}

function find (code) {
	const city = findCity(code | 0);
	
	if(!city) {
		return {city: null, province: null};
	}

	const province = findProvince(city.provinceId);

	return {
		city: city.name || null,
		province: (province ? province.name : null) || null
	};
}

module.exports = {find};
